import requests

# Placeholder class

class DemoHandler(object):

    def __init__(self):
        print('access_token=XXXXX')
        print('Hallo, Welt.')

    def do_nothing(self):
        return

    def do_something(self):
        return 'Ain\'t that something?'

    def some_bool(self):
        return True

    def remote_data(self):
        # import pdb; pdb.set_trace();
        response = requests.get('http://httpbin.org/get')
        return response.text


def main():

    app = DemoHandler()
    print(app.do_something())
    print(app.remote_data())


if __name__ == '__main__':

    main()
