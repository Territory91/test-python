#!/usr/bin/env python3

import os
from setuptools import setup, find_packages

dir_path = os.path.dirname(os.path.realpath(__file__))
# with open(os.path.join(here, 'README.md')) as f:
#    description = f.read()

with open(os.path.join(dir_path, 'version'),"r") as f:
    version = f.readline()

with open(os.path.join(dir_path, 'pjname'),"r") as f:
    pjname = f.readline()

description = "A simple notes app living in the cloud with bare bones functionalities! ☁️🗒\
It's composed by two core and one optional components:\
\
+ 💽 Database (MySQL, MariaDB or SQLite)\
+ 🌐 WebApp\
+ 🖥 Desktop App"

REQUIREMENTS = [
    'requests>=2.6.0',
    'mock>=1.0.1',
    'nose>=1.3.4',
    'nose-cov',
    'nose-mocha-reporter',
    'coverage',
    'tox>=1.9.0',
    'flake8>=2.4.0',
    'pylint',
    'pylint-exit',
    'anybadge',
    'pylama',
]

setup(
    name=pjname,
    version=version,
    description='2019_assignament_DonMel',
    long_description=description,
    author='Salanti Michele, Donati Ivan',
    author_email='m.salanti@campus.unimib.it, i.donati1@campus.unimib.it',
    url='https://gitlab.com/Territory91/2019_assignment1_meldon',
    license="MIT",
    install_requires=REQUIREMENTS,
    include_package_data=True,
    keywords=['app', 'setup.py', 'project', 'tox'],
    packages=find_packages(),
    classifiers=[
        'Programming Language :: Python',
        'Programming Language :: Python :: 3.6',
    ],
    entry_points={
        'console_scripts': ['app = app.app_handler:main']
    },
)
